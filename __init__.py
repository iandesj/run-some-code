from flask import Flask, request, json
from execution.run import Run
import subprocess
from os import listdir
from os.path import isfile, join

app = Flask(__name__, static_url_path='/static')

#TODO: store language associated with the filename in an object, or perhaps DB?
@app.route("/", methods=["POST","GET"])
def execution():
    if request.method=="POST":
        data = request.get_json(force=True)
        # create new script from code and filename
        Run.new_script(data["code"], data["filename"])
        # execute the code with the Run class
        run = Run.execute(data["language"], data["filename"])
        # output stdout and stderr
        out, err = run.communicate();
        # return json with stdout and stderr
        return json.jsonify(stdout=out, stderr=err)

    if request.method=="GET":
        return app.send_static_file('index.html')

@app.route("/files")
def file_list():
    # get files in the scripts directory
    files = [ f for f in listdir('execution/scripts') ]

    return json.jsonify({'files': files})

@app.route("/files/<filename>")
def get_file_contents(filename):
    return Run.script_contents(filename)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
