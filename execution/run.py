from subprocess import (
    Popen, 
    PIPE
)
import os
script_dir = os.path.dirname(__file__)

class Run:

    @classmethod
    def get_script_path(self, fname):
        rel_filepath = "scripts/"+fname
        abs_filepath = os.path.join(script_dir, rel_filepath)
        return abs_filepath

    @staticmethod
    def execute(lang, fname):
        abs_script_path = Run.get_script_path(fname)
        sub_proc = Popen([lang.lower(), abs_script_path],
                                            stdout=PIPE,
                                            stdin=PIPE,
                                            stderr=PIPE)
        return sub_proc

    @staticmethod
    def new_script(code, fname):
        abs_script_path = Run.get_script_path(fname)
        f = open(abs_script_path, 'w')
        f.write(code)
        f.close()

    @staticmethod
    def script_contents(fname):
        try:
            abs_script_path = Run.get_script_path(fname)
            with open(abs_script_path, 'r') as fin:
                return fin.read()
        except IOError as e:
            return "I/O error({0}): {1}".format(e.errno, e.strerror)


